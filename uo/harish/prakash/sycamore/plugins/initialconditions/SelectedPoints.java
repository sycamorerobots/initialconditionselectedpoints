/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uo.harish.prakash.sycamore.plugins.initialconditions;

import it.diunipi.volpi.sycamore.engine.Point2D;
import it.diunipi.volpi.sycamore.engine.SycamoreEngine;
import it.diunipi.volpi.sycamore.engine.SycamoreRobotMatrix;
import it.diunipi.volpi.sycamore.gui.SycamorePanel;
import it.diunipi.volpi.sycamore.plugins.initialconditions.InitialConditionsImpl;
import java.util.ArrayList;
import net.xeoh.plugins.base.annotations.PluginImplementation;

/**
 *
 * @author harry
 */
@PluginImplementation
public class SelectedPoints extends InitialConditionsImpl<Point2D> {

	private SelectedPointsConfigurationPanel	configurationPanel;
	private int									generate	= 0;

	@Override
	public Point2D nextStartingPoint(SycamoreRobotMatrix<Point2D> robots) {

		ArrayList<Point2D> points = getConfigurationPanel().getPoints();
		return (points.size() < 1) ? null : points.get(generate++ % points.size());
	}

	@Override
	public SycamoreEngine.TYPE getType() {

		return SycamoreEngine.TYPE.TYPE_2D;
	}

	@Override
	public String getAuthor() {

		return "Harish Prakash";
	}

	@Override
	public String getPluginShortDescription() {

		return "Place robots at selected positions";
	}

	@Override
	public String getPluginLongDescription() {

		// @formatter:off
		return "Use the textarea below to enter your points. "
				+ "This is a sample, \"1,1;2,2;3,3\" "
				+ "will place robots at positions (1,1), (2,2) and (3,3)";
		// @formatter:on
	}

	@Override
	public SycamorePanel getPanel_settings() {

		return getConfigurationPanel();
	}

	private SelectedPointsConfigurationPanel getConfigurationPanel() {

		if (configurationPanel == null)
			configurationPanel = SelectedPointsConfigurationPanel.getUniqueInstance();
		return configurationPanel;
	}

}
