/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uo.harish.prakash.sycamore.plugins.initialconditions;

import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JTextArea;
import it.diunipi.volpi.sycamore.engine.Point2D;
import it.diunipi.volpi.sycamore.engine.SycamoreEngine;
import it.diunipi.volpi.sycamore.gui.SycamorePanel;

/**
 * Selected Points Configuration Panel. The Selected Points Configuration Panel
 * lets you select the initial positions the robots take based on specific
 * points you have given. If there are more robots than the number of Points,
 * then the n+1 th point will have the same position as the 1st point and so on.
 *
 * @author Harish Prakash
 */
public class SelectedPointsConfigurationPanel extends SycamorePanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JTextArea	textPoints;
	private boolean		changeLock;

	private ArrayList<Point2D> selectedPoints;

	private static SelectedPointsConfigurationPanel uniqueInstance;

	@SuppressWarnings("rawtypes")
	@Override
	public void setAppEngine(SycamoreEngine appEngine) {}

	@Override
	public void updateGui() {}

	@Override
	public void reset() {}

	private SelectedPointsConfigurationPanel() {
		setupGUI();
	}

	private void setupGUI() {

		BoxLayout layout = new BoxLayout(this, BoxLayout.Y_AXIS);
		setLayout(layout);

		add(getTextPoints());
		setBorder(BorderFactory.createTitledBorder("Enter Points"));
	}

	/**
	 * @return the textPoints
	 */
	private JTextArea getTextPoints() {

		if (textPoints == null) {
			Dimension size = new Dimension(400, 200);

			textPoints = new JTextArea();
			textPoints.setPreferredSize(size);
			textPoints.setMinimumSize(size);
			textPoints.addKeyListener(new KeyListener() {

				@Override
				public void keyTyped(KeyEvent e) {

					setLocked(false);

				}

				@Override
				public void keyReleased(KeyEvent e) {

					setLocked(false);

				}

				@Override
				public void keyPressed(KeyEvent e) {

					setLocked(false);

				}
			});
		}

		return textPoints;
	}

	/**
	 * @return the changeLock
	 */
	private boolean isLocked() {

		return changeLock;
	}

	/**
	 * @param changeLock
	 *            the changeLock to set
	 */
	private void setLocked(boolean changeLock) {

		this.changeLock = changeLock;
	}

	/**
	 * @return the selectedPoints
	 */
	private ArrayList<Point2D> getSelectedPoints() {

		if (selectedPoints == null) {
			selectedPoints = new ArrayList<>();
		}
		return selectedPoints;
	}

	public ArrayList<Point2D> getPoints() {

		if (isLocked()) {
			return getSelectedPoints();
		}
		else {
			getSelectedPoints().clear();
			setLocked(true);
		}

		String userInput = getTextPoints().getText();

		try {
			userInput = userInput.replace(" ", "");
			userInput = userInput.replace("\r\n", "\n");
			userInput = userInput.replace("\n\r", "\n");
			userInput = userInput.replace("\n", ";");
			String[] tokens = userInput.split(";");

			for (String token : tokens) {
				// @formatter:off
				try { getSelectedPoints().add(renderToken(token)); }
				catch (NumberFormatException | ArrayIndexOutOfBoundsException | NullPointerException ex) {}
				// @formatter:on
			}
		}
		catch (NullPointerException ex) {}

		return getSelectedPoints();
	}

	private Point2D renderToken(String token) {

		String[] coordinates = token.split(",");
		Point2D point = new Point2D(Float.parseFloat(coordinates[0]), Float.parseFloat(coordinates[1]));
		return point;
	}

	/**
	 * @return the uniqueInstance
	 */
	public static SelectedPointsConfigurationPanel getUniqueInstance() {

		if (uniqueInstance == null) {
			uniqueInstance = new SelectedPointsConfigurationPanel();
		}
		return uniqueInstance;
	}
}
